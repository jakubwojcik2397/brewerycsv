package com.cdq.demo.brewery;

import com.cdq.demo.brewery.model.Brewery;
import com.cdq.demo.brewery.model.BreweryCitiesResult;
import com.cdq.demo.brewery.model.BreweryStateResult;
import com.cdq.demo.brewery.model.dto.BreweryWinePercentageDTO;
import com.cdq.demo.brewery.model.projection.BreweryWineResult;
import com.cdq.demo.brewery.service.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RequestMapping("/api")
@RestController()
public class BreweryController {

    private final BreweryService breweryService;

    @Autowired
    public BreweryController(BreweryService breweryService) {
        this.breweryService = breweryService;
    }

    @GetMapping(value = "/getCountBreweriesEachState")
    public List<BreweryStateResult> getNumberOfBreweriesEachState() {
        return  breweryService.getNumberOfBreweriesEachState();
    }

    @GetMapping(value = "/topCities/{numberOfCities}")
    public List<BreweryCitiesResult> getTopCities(@PathVariable int numberOfCities) {
        return breweryService.getTopCities(numberOfCities);
    }

    @GetMapping(value = "/countBreweriesContainWebsite")
    public long getCountOfBreweriesContainsWebsite() {
        return breweryService.getCountOfBreweriesContainsWebsite();
    }

    @GetMapping(value = "/getListOfBreweriesOffer/{dish}/by/{city}")
    public List<String> getListOfBreweriesOfferDishByCity(@PathVariable String dish, @PathVariable String city) {
        return breweryService.getListOfBreweriesOfferDishByCity(city, dish);
    }

    @GetMapping(value = "/getCountOfBreweryContainWine")
    public List<BreweryWinePercentageDTO> getCountOfBreweryContainWine() {
        return  breweryService.getCountOfBreweryContainWine();
    }
}
