package com.cdq.demo.brewery;

import com.cdq.demo.brewery.model.Brewery;
import com.cdq.demo.brewery.model.State;
import com.cdq.demo.brewery.service.BreweryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.cdq.demo.util.VariableUtil.ADDRESS;
import static com.cdq.demo.util.VariableUtil.BREWERY_CSV;
import static com.cdq.demo.util.VariableUtil.CATEGORIES;
import static com.cdq.demo.util.VariableUtil.CITY;
import static com.cdq.demo.util.VariableUtil.COUNTRY;
import static com.cdq.demo.util.VariableUtil.HOURS;
import static com.cdq.demo.util.VariableUtil.ID;
import static com.cdq.demo.util.VariableUtil.KEYS;
import static com.cdq.demo.util.VariableUtil.LATITUDE;
import static com.cdq.demo.util.VariableUtil.LONGITUDE;
import static com.cdq.demo.util.VariableUtil.MENUS;
import static com.cdq.demo.util.VariableUtil.NAME;
import static com.cdq.demo.util.VariableUtil.POSTAL_CODE;
import static com.cdq.demo.util.VariableUtil.PROVINCE;
import static com.cdq.demo.util.VariableUtil.REGEX_COMMA;
import static com.cdq.demo.util.VariableUtil.REGEX_TO_SEPARATE_DATA_TO_CELL;
import static com.cdq.demo.util.VariableUtil.TWITTER;
import static com.cdq.demo.util.VariableUtil.WEBSITES;

@Configuration
public class BreweryInitialize {
    private String[] columns;
    Logger logger = LoggerFactory.getLogger(BreweryInitialize.class);

    @Autowired
    BreweryService breweryService;

    /*
    * TODO:
    *  - find solution for loadData from liquibase with mapping some JSON column to object
    *  - Create ObjectMapper/Mapstruct bean with brewery map method
    * */
    @PostConstruct
    private void init() {
        logger.info("Intialize date from: " + BREWERY_CSV);

        try (BufferedReader br = new BufferedReader(new FileReader(BREWERY_CSV))) {
            String line;
            List<String> records = new ArrayList<>();
            this.columns = br.readLine().split(REGEX_COMMA);
            while ((line = br.readLine()) != null) {
                records.add(line);
            }

            List<Map<String, String>> maps = assignDataToColumn(records);
            List<Brewery> breweries = maps.stream().map(map -> Brewery.builder()
                    .id(map.get(ID))
                    .menus(map.get(MENUS))
                    .openBrewery(map.get(HOURS))
                    .address(map.get(ADDRESS))
                    .categories(map.get(CATEGORIES))
                    .city(map.get(CITY))
                    .country(map.get(COUNTRY))
                    .keys(map.get(KEYS))
                    .latitude(map.get(LATITUDE))
                    .longitude(map.get(LONGITUDE))
                    .name(map.get(NAME))
                    .province(map.get(PROVINCE))
                    .twitter(map.get(TWITTER))
                    .websites(map.get(WEBSITES))
                    .postalCode(map.get(POSTAL_CODE))
                    .build())
                    .peek(brewery -> brewery.setState(getState(brewery)))
                    .toList();

            breweryService.init(breweries);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Map<String, String>> assignDataToColumn(List<String> breweriesList) {
        List<String[]> separatedData = breweriesList.stream()
                .map(BreweryInitialize::separate)
                .toList();

        return separatedData.stream()
                .map(cell ->
                        IntStream.range(0, columns.length)
                                .boxed()
                                .collect(Collectors.toMap(this::getColumn, i -> cell[i]))
                )
                .toList();
    }

    private static String[] separate(String dataToSeparate) {
        return Pattern.compile(REGEX_TO_SEPARATE_DATA_TO_CELL)
                .matcher(dataToSeparate)
                .results()
                .map(matchResult -> matchResult.group(1))
                .toArray(String[]::new);
    }

    private String getColumn(int index) {
        return columns[index];
    }

    private State getState(Brewery brewery) {

        State rangeByPostalCode = State.getRange(brewery.getPostalCode());
        State rangeByProvince = State.getRange(brewery.getProvince());

        return rangeByPostalCode != null ? rangeByPostalCode : rangeByProvince;
    }
}
