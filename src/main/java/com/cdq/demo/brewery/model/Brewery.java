package com.cdq.demo.brewery.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

import static com.cdq.demo.util.VariableUtil.REGEX_CHAR;
import static com.cdq.demo.util.VariableUtil.REGEX_REPLACEMENT_QOUTE;
import static com.cdq.demo.util.VariableUtil.REGEX_SLASH;
import static com.cdq.demo.util.VariableUtil.REGEX_TARGET_DOUBLE_QOUTE;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Brewery {

    @Id
    private String id;

    private String address;

    @Column(length = 100000)
    private String categories;

    private String city;

    private String country;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Column(name = "hours")
    private List<OpenBrewery> openBrewery;

    @Column(length = 100000)
    private String keys;

    private String latitude;

    private String longitude;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Column(name = "menus")
    private List<Menu> menus;

    @Column(length = 100000)
    private String name;

    private String postalCode;

    private String province;

    private String twitter;

    @Column(length = 500000)
    private String websites;

    @Enumerated(EnumType.STRING)
    private State state;

    public static class BreweryBuilder {

        private List<Menu> menus;
        private List<OpenBrewery> openBrewery;

        public BreweryBuilder menus(String menusJson) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                if (menusJson.isEmpty() | menusJson.isBlank() | !menusJson.startsWith("\"[")) {
                    menus = null;
                    return this;
                }
                menusJson = menusJson.replace(REGEX_SLASH, REGEX_CHAR); // some word in description has quote character

                menusJson = menusJson.replace(REGEX_TARGET_DOUBLE_QOUTE, REGEX_REPLACEMENT_QOUTE);
                menusJson = menusJson.substring(1, menusJson.length() - 1);
                menus = mapper.readValue(menusJson,
                        new TypeReference<>() {
                        });
            } catch (JsonProcessingException e) {
                menus = new ArrayList<>();
            }

            return this;
        }

        public BreweryBuilder openBrewery(String hoursJson) {
            ObjectMapper mapper = new ObjectMapper();
            try {

                if (hoursJson.isEmpty() | hoursJson.isBlank()) {
                    openBrewery = null;
                    return this;
                }
                hoursJson = hoursJson.replace(REGEX_TARGET_DOUBLE_QOUTE, REGEX_REPLACEMENT_QOUTE);
                hoursJson = hoursJson.substring(1, hoursJson.length() - 1);
                openBrewery = mapper.readValue(hoursJson,
                        new TypeReference<>() {
                        });
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

            return this;
        }

    }
}
