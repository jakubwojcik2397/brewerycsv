package com.cdq.demo.brewery.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BreweryCitiesResult {

    private String city;
    private long count;
}
