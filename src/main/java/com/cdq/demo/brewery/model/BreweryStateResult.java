package com.cdq.demo.brewery.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BreweryStateResult {

    private State state;
    private long count;

}
