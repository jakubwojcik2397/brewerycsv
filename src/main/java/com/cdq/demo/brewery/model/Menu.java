package com.cdq.demo.brewery.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;

@Setter
@Getter
@Entity
@Table
public class Menu{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    public double amountMax;

    public double amountMin;

    public String currency;

    public String category;

    public ArrayList<Date> dateSeen;

    @Column(length = 100000)
    public String name;

    @Column(length = 100000)
    public ArrayList<String> sourceURLs;

    @Column(length = 100000)
    public String description;

}