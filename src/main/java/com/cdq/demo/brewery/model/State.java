package com.cdq.demo.brewery.model;

import java.util.Arrays;

import static com.cdq.demo.util.VariableUtil.CODE_4_REGEX;

public enum State {

    AL(35000, 36999, "Alabama"),
    AK(99500, 99999, "Alaska"),
    AZ(85000, 86999, "Arizona"),
    AR(71600, 72999, "Arkansas"),
    CA(90000, 96699, "California"),
    CO(80000, 81999, "Colorado"),
    CT(6000, 6999, "Connecticut"),
    DE(19700, 19999, "Delaware"),
    FL(32000, 34999, "Florida"),
    GA(30000, 31999, "Georgia"),
    HI(96700, 96999, "Hawaii"),
    ID(83200, 83999, "Idaho"),
    IL(60000, 62999, "Illinois"),
    IN(46000, 47999, "Indiana"),
    IA(50000, 52999, "Iowa"),
    KS(66000, 67999, "Kansas"),
    KY(40000, 42999, "Kentucky"),
    LA(70000, 71599, "Louisiana"),
    ME(3900, 4999, "Maine"),
    MD(20600, 21999, "Maryland"),
    MA(1000, 2799, "Massachusetts"),
    MI(48000, 49999, "Michigan"),
    MN(55000, 56999, "Minnesota"),
    MS(38600, 39999, "Mississippi"),
    MO(63000, 65999, "Missouri"),
    MT(59000, 59999, "Montana"),
    NC(27000, 28999, "North Carolina"),
    ND(58000, 58999, "North Dakota"),
    NE(68000, 69999, "Nebraska"),
    NV(88900, 89999, "Nevada"),
    NH(3000, 3899, "New Hampshire"),
    NJ(7000, 8999, "New Jersey"),
    NM(87000, 88499, "New Mexico"),
    NY(10000, 14999, "New York"),
    OH(43000, 45999, "Ohio"),
    OK(73000, 74999, "Oklahoma"),
    OR(97000, 97999, "Oregon"),
    PA(15000, 19699, "Pennsylvania"),
    PR(300, 999, "Puerto Rico"),
    RI(2800, 2999, "Rhode Island"),
    SC(29000, 29999, "South Carolina"),
    SD(57000, 57999, "South Dakota"),
    TN(37000, 38599, "Tennessee"),
    TX(75000, 79999, "Texas"),
    TX_SECOND(88500, 88599, "Texas"),
    UT(84000, 84999, "Utah"),
    VT(5000, 5999, "Vermont"),
    VA(22000, 24699, "Virgina"),
    DC(20000, 20599, "Washington DC"),
    DC_SECOND(20000, 20599, "District of Columbia"),
    WA(98000, 99499, "Washington"),
    WV(24700, 26999, "West Virginia"),
    WI(53000, 54999, "Wisconsin"),
    WY(82000, 83199, "Wyoming");

    private final int min;
    private final int max;
    private final String state;

    private State(int min, int max, String state) {
        this.min = min;
        this.max = max;
        this.state = state;
    }

    public String getStateRange() {
        return String.format(state);
    }

    public static State getRangePostalCode(String postalCode) {
        String replaced = postalCode.replaceFirst(CODE_4_REGEX, "");
        if(!replaced.matches("\\d+")) {
            return null;
        }
        int value = tryParse(replaced);

        return Arrays.stream(values())
                .filter(r -> value >= r.min && value <= r.max)
                .findFirst()
                .orElse(null);
    }

    public static State getRange(String stateName) {
        if (stateName.length() > 2) {
            return Arrays.stream(values())
                    .filter(r -> r.state.equals(stateName))
                    .findFirst()
                    .orElse(getRangePostalCode(stateName));
        }

        return Arrays.stream(values())
                .filter(r -> r.name().equals(stateName))
                .findFirst()
                .orElse(null);
    }

    private static Integer tryParse(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

}