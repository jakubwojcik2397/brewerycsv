package com.cdq.demo.brewery.model.dto;

import com.cdq.demo.brewery.model.State;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
public class BreweryWinePercentageDTO {

    private State state;
    private BigDecimal percentageContainWine;

}
