package com.cdq.demo.brewery.model.projection;

import com.cdq.demo.brewery.model.State;

public interface BreweryWineResult {

    State getState();
    long getCount();

}
