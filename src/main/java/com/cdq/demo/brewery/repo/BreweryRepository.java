package com.cdq.demo.brewery.repo;

import com.cdq.demo.brewery.model.Brewery;
import com.cdq.demo.brewery.model.BreweryCitiesResult;
import com.cdq.demo.brewery.model.BreweryStateResult;
import com.cdq.demo.brewery.model.projection.BreweryWineResult;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BreweryRepository extends CrudRepository<Brewery, String> {

    @Query(value = "SELECT new com.cdq.demo.brewery.model.BreweryStateResult(state, COUNT(state)) \n" +
            "FROM Brewery \n" +
            "GROUP BY state\n" +
            "ORDER BY COUNT(state) DESC")
    List<BreweryStateResult> getNumberOfBreweriesEachState();

    @Query(value = "SELECT new com.cdq.demo.brewery.model.BreweryCitiesResult(city, COUNT(city)) \n" +
            "FROM Brewery \n" +
            "GROUP BY city \n" +
            "ORDER BY COUNT(city) DESC")
    List<BreweryCitiesResult> getTopFiveCities();

    @Query(value = "SELECT COUNT(WEBSITES) FROM Brewery WHERE WEBSITES <> ''")
    long getCountOfBreweriesContainsWebsite();

    @Query(value = "SELECT b.name FROM Brewery b \n" +
            "LEFT JOIN BREWERY_MENUS bm \n" +
            "ON b.Id = BM.BREWERY_ID \n" +
            "LEFT JOIN Menu m \n" +
            "ON bm.MENUS_ID = m.id \n" +
            "WHERE LOWER(b.city) = :city AND (LOWER(m.name) like '%' || :dish || '%' " +
                "OR LOWER(m.name) like '%' || :dish || ' %')", nativeQuery = true)
    List<String> getListOfBreweriesOfferDishByCity(@Param("city") String city, @Param("dish") String dish);

    @Query(value = "SELECT b.state, COUNT(b.state) as count FROM BREWERY B \n" +
            "LEFT JOIN BREWERY_MENUs BM \n" +
            "ON B.Id = BM.BREWERY_ID\n" +
            "LEFT JOIN MENU M\n" +
            "ON BM.MENUS_ID = m.id\n" +
            "WHERE LOWER(M.Name) LIKE '% wine %' OR LOWER(M.Name) LIKE 'wine %' OR LOWER(M.Name) LIKE '% wine'\n" +
            "OR LOWER(B.Name) LIKE '% wine %' OR LOWER(B.Name) LIKE 'wine %' OR LOWER(B.Name) LIKE '% wine'\n" +
            "OR LOWER(B.CATEGORIES) LIKE '% wine %' OR LOWER(B.CATEGORIES) LIKE 'wine %' OR LOWER(B.CATEGORIES) LIKE '% wine'\n" +
            "GROUP BY STATE\n" +
            "ORDER BY COUNT(STATE) DESC", nativeQuery = true)
    List<BreweryWineResult> getCountOfBreweryContainWine();


}
