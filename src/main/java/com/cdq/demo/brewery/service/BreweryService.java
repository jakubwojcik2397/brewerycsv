package com.cdq.demo.brewery.service;

import com.cdq.demo.brewery.model.Brewery;
import com.cdq.demo.brewery.model.BreweryCitiesResult;
import com.cdq.demo.brewery.model.BreweryStateResult;
import com.cdq.demo.brewery.model.dto.BreweryWinePercentageDTO;
import com.cdq.demo.brewery.model.projection.BreweryWineResult;
import com.cdq.demo.brewery.repo.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreweryService {

    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryService(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    public void init(List<Brewery> breweryList) {
        breweryRepository.saveAll(breweryList);
    }

    public List<BreweryStateResult> getNumberOfBreweriesEachState() {
        return breweryRepository.getNumberOfBreweriesEachState();
    }

    public List<BreweryCitiesResult> getTopCities(int topCities) {
        return breweryRepository.getTopFiveCities().stream()
                .limit(topCities).collect(Collectors.toList());
    }

    public long getCountOfBreweriesContainsWebsite() {
        return breweryRepository.getCountOfBreweriesContainsWebsite();
    }

    public List<String> getListOfBreweriesOfferDishByCity(String city,String dish) {
        return breweryRepository.getListOfBreweriesOfferDishByCity(city.toLowerCase(), dish.toLowerCase());
    }

    public List<BreweryWinePercentageDTO> getCountOfBreweryContainWine() {
        List<BreweryWineResult> containWine = breweryRepository.getCountOfBreweryContainWine();
        List<BreweryStateResult> numberOfBreweriesEachState = getNumberOfBreweriesEachState();

        List<BreweryWinePercentageDTO> breweryWinePercentageDTOS = containWine.stream().map(breweryWineResult -> {
            BreweryStateResult br = numberOfBreweriesEachState.stream()
                    .filter(breweryStateResult -> breweryStateResult.getState().equals(breweryWineResult.getState()))
                    .findFirst().orElseThrow(null);
            if (br == null) {
                return new BreweryWinePercentageDTO(breweryWineResult.getState(), new BigDecimal(0));
            }
            BigDecimal numberOfWine = BigDecimal.valueOf(breweryWineResult.getCount());
            BigDecimal numberOfStates = BigDecimal.valueOf(br.getCount());
            BigDecimal result = numberOfWine.divide(numberOfStates, 4, RoundingMode.HALF_UP);
            return new BreweryWinePercentageDTO(breweryWineResult.getState(), result);
        }).toList();

        return breweryWinePercentageDTOS;
    }

}
