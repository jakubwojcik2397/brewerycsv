package com.cdq.demo.util;

public final class VariableUtil {

    public static String REGEX_TO_SEPARATE_DATA_TO_CELL = "(?<=^|,)((\"[^\"]*\")|((\\\"\\[\\{\\\"(.+?)\\\":(.+?)}{1,}]\\\"))|([^,]*)|)(?=$|,)";
    public static final String REGEX_COMMA = ",";
    public static final String REGEX_SLASH = "\\\\\"\"";
    public final static String CODE_4_REGEX = "(-(.*))";

    public static final String REGEX_CHAR = "'";
    public static final String REGEX_TARGET_DOUBLE_QOUTE = "\"\"";
    public static final String REGEX_REPLACEMENT_QOUTE = "\"";
    public static String BREWERY_CSV = "src/main/resources/liquibase/data/breweries_usa.csv";
    public static final String ID = "id";
    public static final String MENUS = "menus";
    public static final String HOURS = "hours";
    public static final String ADDRESS = "address";
    public static final String CATEGORIES = "categories";
    public static final String CITY = "city";
    public static final String COUNTRY = "country";
    public static final String KEYS = "keys";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String NAME = "name";
    public static final String PROVINCE = "province";
    public static final String TWITTER = "twitter";
    public static final String WEBSITES = "websites";
    public static final String POSTAL_CODE = "postalCode";

}
